import java.io.Serializable;
import java.util.Optional;

public class Person implements Serializable{

    private String name;
    private String lastName;
    private Integer age;
    private Address address;

    public Person() {
    }

    public Person(String name, String lastName, Integer age) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + Optional.ofNullable(name).orElse("") +
                ", lastName='" + Optional.ofNullable(lastName).orElse("") +
                ", age=" + Optional.ofNullable(age).orElse(0) +
                ", address=" + address +
                '}';
    }
}