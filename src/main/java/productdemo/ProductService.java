package productdemo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ProductService {

    @Autowired
    private ProductBuilder productBuilder;

    public String doSth(String name, Integer price, String type){
        productBuilder.build(name, price, type);
        String proccesed = productBuilder.processA();
        log.debug("Do sth " + proccesed);
        return "Do sth " + proccesed;
    }
}
