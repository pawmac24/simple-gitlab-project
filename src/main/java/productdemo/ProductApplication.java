package productdemo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class ProductApplication implements CommandLineRunner{

    @Autowired
    private ProductService productService;

    @Override
    public void run(String... args) {
        String result = productService.doSth("Product 01", 12, "Type 1");
        System.out.println(result);
        log.debug("result " + result);
    }

    public static void main(String[] args) {
        SpringApplication.run(ProductApplication.class, args);
    }
}