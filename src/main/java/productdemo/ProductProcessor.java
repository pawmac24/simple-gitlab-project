package productdemo;

public class ProductProcessor {

    private ProductBuilder productBuilder;

    public ProductProcessor(ProductBuilder productBuilder) {
        this.productBuilder = productBuilder;
    }

    public String process(){
        Product product = this.productBuilder.getProduct();
        return "processed" + " " +  product.getName();
    }
}
