package productdemo;

import lombok.Getter;
import org.springframework.stereotype.Component;

@Component
@Getter
public class ProductBuilder {

    private Product product;

    public void build(String name, Integer price, String type){
        Product product = new Product();
        product.setName(name);
        product.setPrice(price);
        product.setType(new Type(type));
        this.product = product;
    }

    public String processA(){
        ProductProcessor productProcessor = new ProductProcessor(this);
        return productProcessor.process();
    }
}
