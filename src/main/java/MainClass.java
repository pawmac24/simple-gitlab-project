import org.apache.commons.lang3.StringUtils;

public class MainClass {

    public static void main(String[] args) {
        long numbers[] = {1, 2, 3};
        String numberJoinString = StringUtils.join(numbers, ';');
        System.out.printf("%s\n", numberJoinString);

        System.out.printf("%s\n", "Hello, it is a nice day");
        System.out.printf("%s\n", "Hello my darling,  it is a nice day");
    }
}
