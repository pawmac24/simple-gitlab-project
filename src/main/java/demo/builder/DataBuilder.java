package demo.builder;

import demo.dto.ShipmentDto;
import demo.service.ExampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalTime;

@Component
public class DataBuilder {

    @Autowired
    private ExampleService exampleService;

    public ShipmentDto buildShipment(){

        ShipmentDto shipmentDto = new ShipmentDto();
        shipmentDto.setShipmentId(1L);
        shipmentDto.setShipmentDate(LocalDate.now());
        shipmentDto.setShipmentTime(LocalTime.now());
        shipmentDto.setParcelCount(1);
        ShipmentDto.ParcelDto parcelDto = new ShipmentDto.ParcelDto();
        parcelDto.setParcelNo(exampleService.findParcelNumber());
        shipmentDto.setParcelDto(parcelDto);

        return shipmentDto;
    }
}
