package demo.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class HelloWorldService {

    @Value("${city:Gdansk}")
    private String city;

    @Value("${firstName}")
    private String firstName;

    @Value("${lastName}")
    private String lastName;

    public String getHelloMessage() {
        return "Hello " + firstName + " " + lastName + " in " + city;
    }
}
