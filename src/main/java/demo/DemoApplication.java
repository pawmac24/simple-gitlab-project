package demo;

import demo.builder.DataBuilder;
import demo.dto.ShipmentDto;
import demo.service.HelloWorldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner {

	@Autowired
	private HelloWorldService helloWorldService;

	@Autowired
    private DataBuilder dataBuilder;

	@Override
	public void run(String... args) {
		System.out.println(this.helloWorldService.getHelloMessage());

        ShipmentDto shipmentDto = dataBuilder.buildShipment();
		System.out.println(shipmentDto.toString());
	}
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}
