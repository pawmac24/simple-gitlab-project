package demo.dto;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
public class ShipmentDto {
    private Long shipmentId;
    private LocalDate shipmentDate;
    private LocalTime shipmentTime;
    private long parcelCount;

    private ParcelDto parcelDto;

    @Data
    public static class ParcelDto{
        private String parcelNo;
    }
}
