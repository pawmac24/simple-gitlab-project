import java.util.List;

public class PersonUpdater {

    public void updatePerson(Person person, String name, String lastName, Integer age){
        person.setName(name);
        person.setLastName(lastName);
        person.setAge(age);
    }

    public void updatePersonList(List<Person> personList, Person person){
        personList.add(person);
    }
}
