import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

public class Java8TestClass {

    @Test
    public void testMapFunction() {
        List<String> alpha = Arrays.asList("a", "b", "c", "d");
        List<String> alphaUpperList = alpha.stream().map(String::toUpperCase).collect(Collectors.toList());

        assertEquals("A", alphaUpperList.get(0));
        assertEquals("B", alphaUpperList.get(1));
        assertEquals("C", alphaUpperList.get(2));
        assertEquals("D", alphaUpperList.get(3));
        assertEquals(4, alphaUpperList.size());
    }

    @Test
    public void testMapFunctionAndSortWithObjects() {
        List<Person> persons = new ArrayList<>();

        Person person = new Person("Paul", "McCartney", 77);
        persons.add(person);

        person = new Person("Andrew", "Warchol", 87);
        persons.add(person);

        person = new Person("Lucy", "Barrow", 45);
        persons.add(person);

        List<String> names = persons.stream().map(Person::getName).sorted().collect(Collectors.toList());
        assertEquals("Andrew", names.get(0));
        assertEquals("Lucy", names.get(1));
        assertEquals("Paul", names.get(2));
    }

    @Test
    public void testStreamCollectors() {
        List<String> givenCollection = Arrays.asList("a1", "a2", "b1", "c2", "c1");
        List<String> expectedCollection = Arrays.asList("C1", "C2");

        List<String> resultCollection = givenCollection.stream()
                .filter(s -> s.startsWith("c"))
                .map(String::toUpperCase)
                .sorted()
                .collect(Collectors.toList());

        assertEquals(expectedCollection, resultCollection);
    }

    @Test
    public void testOptionalsInStream() {
        List<String> givenCollection = Arrays.asList("a1", "a2", "a3");
        String resultElement = givenCollection.stream()
                .filter(s -> s.startsWith("c"))
                .findFirst()
                .orElse(null);
        assertEquals(null, resultElement);

        givenCollection = Arrays.asList("a1", "b2", "c3");
        resultElement = givenCollection.stream()
                .filter(s -> s.startsWith("c"))
                .findFirst()
                .orElse(null);
        assertEquals("c3", resultElement);
    }

    @Test
    public void testMapToObject(){
        Stream.of(1.0, 2.0, 3.0)
                .mapToInt(Double::intValue)
                .mapToObj(i -> "a" + i)
                .forEach(System.out::println);
    }
}
