import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class PersonUpdaterTest {
    @Test
    public void testUpdatePerson() throws Exception {
        PersonUpdater personUpdater = new PersonUpdater();
        Person person =  new Person("Paul", "McCartney", 77);
        personUpdater.updatePerson(person, "Robert", "Deniro", 100);

        assertEquals("Robert", person.getName());
        assertEquals("Deniro", person.getLastName());
        assertEquals(100, (int)person.getAge());
    }

    @Test
    public void testUpdatePersonList() throws Exception {
        PersonUpdater personUpdater = new PersonUpdater();

        List<Person> persons = new ArrayList<>();

        Person person = new Person("Paul", "McCartney", 77);
        personUpdater.updatePersonList(persons, person);

        person = new Person("Andrew", "Warchol", 87);
        personUpdater.updatePersonList(persons, person);

        person = new Person("Lucy", "Barrow", 45);
        personUpdater.updatePersonList(persons, person);

        assertEquals(3, persons.size());
        assertEquals("McCartney", persons.get(0).getLastName());
        assertEquals("Warchol", persons.get(1).getLastName());
        assertEquals("Barrow", persons.get(2).getLastName());
    }

}