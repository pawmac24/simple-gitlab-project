package productdemo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {

    @InjectMocks
    private ProductService productService; //test service

    @Mock
    private ProductBuilder productBuilder;

    @Test
    public void testDoSth(){
        //given
        doCallRealMethod().when(productBuilder).build(any(String.class), any(Integer.class), any(String.class));
        when(productBuilder.getProduct()).thenCallRealMethod();
        when(productBuilder.processA()).thenCallRealMethod();

        //when
        String resultString = productService.doSth("PRODUCT A", 2, "TYPE A");

        //then
        assertEquals("Do sth processed PRODUCT A", resultString);
    }
}