import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

public class TestClass {

    @Test
    public void testStringUtilsJoin(){
        long numbers[] = {1, 2, 3};
        String expectedJoinString = "1;2;3";
        String numberJoinString = StringUtils.join(numbers, ';');
        assertEquals(expectedJoinString, numberJoinString);
    }

    @Test
    public void testStringUtilsSplit(){
        String exampleString = "a,b,c";
        String[] splitStringArray = StringUtils.split(exampleString, ',');
        String[] expectedSplitStringArray = {"a","b","c"};
        assertArrayEquals(expectedSplitStringArray, splitStringArray);
    }

    @Test
    public void testCollectionUtilsIsEmpty(){
        List<String> stringList = new ArrayList<>();
        assertTrue(CollectionUtils.isEmpty(stringList));

        stringList = null;
        assertTrue(CollectionUtils.isEmpty(stringList));
    }

    @Test
    public void testOptional(){
        Person person = new Person();
        person.setName("John");

        assertTrue(Optional.ofNullable(person.getName()).isPresent());
        assertFalse(Optional.ofNullable(person.getLastName()).isPresent());
        assertFalse(Optional.ofNullable(person.getAge()).isPresent());
    }

    @Test
    public void testShallowCopy() throws Exception {
        Person person = new Person();
        person.setName("Paul");
        person.setLastName("McCartney");
        person.setAge(77);
        Address address = new Address();
        address.setCity("Chicago");
        address.setZipCode("22-222");
        address.setStreet("White Street");
        address.setHomeNumber("25S");
        person.setAddress(address);

        Person person2 = (Person) BeanUtils.cloneBean(person);
        assertEquals(person.getName(), person2.getName());
        assertEquals(person.getLastName(), person2.getLastName());
        assertEquals(person.getAge(), person2.getAge());
        assertEquals(person.getAddress().getCity(), person2.getAddress().getCity());

        person.setAge(88);
        assertNotEquals(person.getAge(), person2.getAge());

        person.getAddress().setCity("New York");
        //Here changing city in one object changes the city in other, the same reference
        assertEquals(person.getAddress().getCity(), person2.getAddress().getCity());
    }

    @Test
    public void testDeepCopy() throws Exception {
        Person person = new Person();
        person.setName("Paul");
        person.setLastName("McCartney");
        person.setAge(77);
        Address address = new Address();
        address.setCity("Chicago");
        address.setZipCode("22-222");
        address.setStreet("White Street");
        address.setHomeNumber("25S");
        person.setAddress(address);

        Person person2 = SerializationUtils.clone(person);
        assertEquals(person.getName(), person2.getName());
        assertEquals(person.getLastName(), person2.getLastName());
        assertEquals(person.getAge(), person2.getAge());
        assertEquals(person.getAddress().getCity(), person2.getAddress().getCity());

        person.setAge(88);
        assertNotEquals(person.getAge(), person2.getAge());

        person.getAddress().setCity("New York");
        //Here changing city in one object does not change the city in other (object are independent)
        //requires that objects are Serializable
        assertNotEquals(person.getAddress().getCity(), person2.getAddress().getCity());
    }

    @Test
    public void testTruncateDate(){
        Date currentDateTime = new GregorianCalendar(2017, 11, 7, 11, 30, 15).getTime();
        Date currentDate = DateUtils.truncate(currentDateTime, Calendar.DAY_OF_MONTH);
        assertEquals(new GregorianCalendar(2017, 11, 7).getTime(), currentDate);
    }
}
